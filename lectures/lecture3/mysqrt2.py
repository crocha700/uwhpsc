
"""

    Module to compute square roots

"""

def sqrt2(x,tol=1.e-14):

    '''
    Computes the square root of x using
        Newton's method.
    '''

    sold = 1.       # initial guess
    error = 1.      # initialize error

    while abs(error) > tol:

        s = .5 * (sold + x/sold)
        
        error = s-sold

        sold = s

    return s

def test():

    '''
        test the square root code
    '''
    
    from numpy import sqrt


    sqrt2(4) == 2

